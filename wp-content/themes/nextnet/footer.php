<footer>
  <div class="row rodape">
    <div class="container-fluid">
      <div class="container">
        <div class="resumo-rodape col-xs-12 col-sm-12 col-md-3 col-lg-3">
          <a href="index.html">
            <img src="<?php bloginfo("template_url");?>/img/logo-branco.png" alt="NextNet">
          </a>
        </div>
        <div class="contato-rodape col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <div>
            <b>São Paulo - SP</b>
            <br>
            <br><?php echo get_option("telefone1")?>
            <br><small><?php echo get_option("endereco1")?><br><?php echo get_option("cep1")?></small>
          </div>
          <div>
            <b>Campo Grande - MS</b>
            <br>
            <br><?php echo get_option("telefone2")?>
            <br><small><?php echo get_option("endereco2")?><br><?php echo get_option("cep2")?></small>
          </div>
        </div>
        <div class="midias col-xs-12 col-sm-12 col-md-3 col-lg-3">
          <div class="box-newslatter">
            <h1>Receba nossa newslatter</h1>
            <form action="">
              <input type="text" placeholder="Seu email">
              <input type="submit" value="enviar" class="btn-dark">
            </form>
          </div>
          <ul>
            <li>
              <a href="<?php echo get_option("facebook")?>" target="_blank">
                <i class="icon-facebook3"></i>
              </a>
            </li>
            <li>
              <a href="<?php echo get_option("twitter")?>" target="_blank">
                <i class="icon-twitter3"></i>
              </a>
            </li>
            <li>
              <a href="<?php echo get_option("google")?>" target="_blank">
                <i class="icon-google-plus3"></i>
              </a>
            </li>
            <li>
              <a href="<?php echo get_option("instagram")?>" target="_blank">
                <i class="icon-instagram-with-circle"></i>
              </a>
            </li>
            <li>
              <a href="<?php echo get_option("linkedin")?>" target="_blank">
                <i class="icon-linkedin-with-circle"></i>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="row copy">
    <div class="container-fluid">
      <div class="container">
        <small>© 2015 NextNet - Todos os direitos reservados.</small>
        <a href="http://www.nurimba.com" target="_blank" title="Development by">
          <img src="<?php bloginfo("template_url");?>/img/icone-nurimba.svg" alt="Nurimba - Grupo de desenvolvedores felizes!">
        </a>
      </div>
    </div>
  </div>
</footer>

  <script src="<?php bloginfo("template_url");?>/scripts/vendor-4dce89050d.js"></script>
  <script src="<?php bloginfo("template_url");?>/scripts/app-948f7c4942.js"></script>

  <!--wordpress footer-->
  <?php wp_footer(); ?>
</body>

</html>

