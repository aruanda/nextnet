<?php

	add_theme_support('menus');
	register_nav_menu('navbar', 'Menu Topo');
	add_theme_support( 'post-thumbnails' );

	require_once ( 'posts-types/produto.php' );
	require_once ( 'posts-types/solucao.php' );
	require_once ( 'posts-types/servico.php' );
	require_once ( 'posts-types/internetDasCoisas.php' );
	require_once ( 'posts-types/blog.php' );
	require_once ( 'posts-types/fotos.php' );
	require_once ( 'posts-types/video.php' );
            require_once ( 'posts-types/parceiro.php' );
	require_once ( 'posts-types/cliente.php' );
	require_once ( 'posts-types/menu-nextnet.php' );
	require_once ( 'posts-types/save_post.php' );
	require_once ( 'posts-types/taxonomia_tipo.php' );
	require_once ( 'posts-types/taxonomia_seguimento.php' );
	require_once ( 'posts-types/taxonomia_taxservico.php' );
	require_once ( 'posts-types/taxonomia_taxsolucao.php' );
            require_once ( 'posts-types/taxonomia_categoria.php' );
	require_once ( 'posts-types/taxonomia_destaque.php' );

add_action( 'widgets_init', 'nextnet_sidebar' );

function nextnet_sidebar() {

    register_sidebar(array(
            'name' => 'Texto Topo Capa',
            'id'            => 'sidebar-topo',
            'description'   => __( 'Texto Capa ', 'nextnet' ),
            'before_title' => '<h1>',
            'after_title' => '</h1>',
            'before_widget' => '<p>',
            'after_widget' => '</p>',
    ));

    register_sidebar(array(
            'name' => 'Texto Soluções Capa',
            'id'            => 'sidebar-solucoes',
            'description'   => __( 'Soluções Capa ', 'nextnet' ),
            'before_title' => '<h1>',
            'after_title' => '</h1>',
            'before_widget' => '<p>',
            'after_widget' => '</p>',
    ));

    register_sidebar(array(
            'name' => 'Internet das Coisas Capa',
            'id'            => 'sidebar-internet',
            'description'   => __( 'Internet das Coias ', 'nextnet' ),
            'before_title' => '<h1>',
            'after_title' => '</h1>',
            'before_widget' => '<p>',
            'after_widget' => '</p>',
    ));

   register_sidebar(array(
            'name' => 'Parceiros Capa',
            'id'            => 'sidebar-parceiros',
            'description'   => __( 'Internet das Coias ', 'nextnet' ),
            'before_title' => '<h1>',
            'after_title' => '</h1>',
            'before_widget' => ' <p>',
            'after_widget' => '</p>',
    ));

   register_sidebar(array(
            'name' => 'Clientes Capa',
            'id'            => 'sidebar-clientes',
            'description'   => __( 'Clientes ', 'nextnet' ),
            'before_title' => '<h1>',
            'after_title' => '</h1>',
            'before_widget' => '<p>',
            'after_widget' => '</p>',
    ));

   register_sidebar(array(
            'name' => 'Texto Serviços',
            'id'            => 'sidebar-servicos',
            'description'   => __( 'Serviços ', 'nextnet' ),
            'before_title' => '<h1>',
            'after_title' => '</h1>',
            'before_widget' => '<p>',
            'after_widget' => '</p>',
    ));


}


function contador(){
    $n = 1;
    $limite = 3;
    while ($n <= $limite) {
        $n++;

    }
    return $n-3;
}
















