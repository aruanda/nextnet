<?php
add_action('init', 'taxservico_register');

function taxservico_register(){

	$argsCategoria = array(

			'labels'=>array(
				'name' => __('Grupo de Serviço'),
				'singular_name' => __('taxservico'),
				'add_new' => __('Novo Grupo de Serviço'),
				'add_new_item' => __('Adicionar novo Grupo de Serviço'),
				'edit_item' => __('Editar Grupo de Serviço'),
				'new_item' => __('Novo Grupo de Serviço'),
				'view_item' => __('Ver Grupo de Serviço'),
				'search_items' => __('Buscar Grupo de Serviço'),
			),
			'hierarchical'=>true,


	);

	register_taxonomy('taxservico', array('solucao' , 'servico' ), $argsCategoria);
}