<?php

add_action('init', 'cliente_register');

function cliente_register() {

	$labels = array(
			'name' => __('Clientes'),
			'singular_name' => __('cliente'),
			'add_new' => __('Novo Cliente'),
			'add_new_item' => __('Adicionar novo Cliente'),
			'edit_item' => __('Editar Cliente'),
			'new_item' => __('Nova Cliente'),
			'view_item' => __('Ver Cliente'),
			'search_items' => __('Buscar Cliente'),
			'not_found' =>  __('Nenhuma Cliente encontrada'),
			'not_found_in_trash' => __('Nada encontrado na Lixeira'),
			'parent_item_colon' => ''
	);

	$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position'=>10,
			'supports'=>array('title', 'thumbnail', 'excerpt')


	);

	register_post_type( 'cliente' , $args );

}


