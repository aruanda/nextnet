<?php

add_action('init', 'servico_register');

function servico_register() {

	$labels = array(
			'name' => __('Serviços'),
			'singular_name' => __('servico'),
			'add_new' => __('Novo Serviço'),
			'add_new_item' => __('Adicionar nova Serviço'),
			'edit_item' => __('Editar Serviço'),
			'new_item' => __('Novo Serviço'),
			'view_item' => __('Ver Serviço'),
			'search_items' => __('Buscar Serviço'),
			'not_found' =>  __('Nenhuma Serviço encontrado'),
			'not_found_in_trash' => __('Nada encontrado na Lixeira'),
			'parent_item_colon' => ''
	);

	$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position'=>3,
			'taxonomies'=>array('seguimento', 'taxsolucao', 'taxservico'),
			'supports'=>array('title', 'editor', 'thumbnail', 'excerpt')


	);

	register_post_type( 'servico' , $args );

}

