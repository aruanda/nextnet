<?php
add_action('init', 'tipo_register');

function tipo_register(){

	$argsCategoria = array(

			'labels'=>array(
				'name' => __('Tipo'),
				'singular_name' => __('tipo'),
				'add_new' => __('Novo Tipo'),
				'add_new_item' => __('Adicionar novo Tipo'),
				'edit_item' => __('Editar Tipo'),
				'new_item' => __('Novo Tipo'),
				'view_item' => __('Ver Tipo'),
				'search_items' => __('Buscar Tipo'),
			),
			'hierarchical'=>true,


	);

	register_taxonomy('tipo', array('produto' , ''), $argsCategoria);
}