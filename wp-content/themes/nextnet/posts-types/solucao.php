<?php

add_action('init', 'solucao_register');

function solucao_register() {

	$labels = array(
			'name' => __('Soluções'),
			'singular_name' => __('solucao'),
			'add_new' => __('Nova Solução'),
			'add_new_item' => __('Adicionar nova Solucao'),
			'edit_item' => __('Editar Solução'),
			'new_item' => __('Nova Solução'),
			'view_item' => __('Ver Solução'),
			'search_items' => __('Buscar Solução'),
			'not_found' =>  __('Nenhuma Solução encontrado'),
			'not_found_in_trash' => __('Nada encontrado na Lixeira'),
			'parent_item_colon' => ''
	);

	$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position'=>4,
			'taxonomies'=>array('seguimento', 'taxservico', 'taxsolucao',),
			'supports'=>array('title', 'editor', 'thumbnail', 'excerpt')


	);

	register_post_type( 'solucao' , $args );

}

