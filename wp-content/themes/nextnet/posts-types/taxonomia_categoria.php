<?php
add_action('init', 'categoria_register');

function categoria_register(){

	$argsCategoria = array(

			'labels'=>array(
				'name' => __('Categoria'),
				'singular_name' => __('Categoria'),
				'add_new' => __('Novo Categoria'),
				'add_new_item' => __('Adicionar novo Categoria'),
				'edit_item' => __('Editar Categoria'),
				'new_item' => __('Novo Categoria'),
				'view_item' => __('Ver Categoria'),
				'search_items' => __('Buscar Categoria'),
			),
			'hierarchical'=>true,


	);

	register_taxonomy('categoria', array('blog'), $argsCategoria);
}