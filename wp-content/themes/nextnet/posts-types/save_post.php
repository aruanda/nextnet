<?php

function wpt_save_events_meta($post_id, $post) {

	if(isset($_POST['meta_post'])){

		if( $post->post_type == 'revision' )
			return;

		foreach ($_POST['meta_post'] as $key=>$value){

			if($value){
				if(get_post_meta($post->ID, $key, FALSE)){
					update_post_meta($post->ID, $key, $value);
				}
				else{
					$return = add_post_meta($post->ID, $key, $value);
					
				}
			}else{
				if(!$value)
					delete_post_meta($post->ID, $key);
			}
		}
	}
	else
		return $post->ID;
}

add_action('save_post', 'wpt_save_events_meta', 1, 2);



add_filter( 'subpost_display_column', 'labels_subpost', 10, 3 );

function labels_subpost($post, $subpost, $sub_post_type){

	$function = "labels_subpost_".$subpost->post_type;

	return $function($post, $subpost, $sub_post_type);

}


add_filter( 'subpost_form_fields', 'fields_subpost', 10, 2 );

function fields_subpost($post, $subpost){
	
	$function = "fields_subpost_".$subpost->post_type;

	$function($subpost);

}