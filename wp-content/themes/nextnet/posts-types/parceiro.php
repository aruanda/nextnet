<?php

add_action('init', 'parceiro_register');

function parceiro_register() {

	$labels = array(
			'name' => __('Parceiros'),
			'singular_name' => __('parceiro'),
			'add_new' => __('Novo Parceiro'),
			'add_new_item' => __('Adicionar novo Parceiro'),
			'edit_item' => __('Editar Parceiro'),
			'new_item' => __('Nova Parceiro'),
			'view_item' => __('Ver Parceiro'),
			'search_items' => __('Buscar Parceiro'),
			'not_found' =>  __('Nenhuma Parceiro encontrada'),
			'not_found_in_trash' => __('Nada encontrado na Lixeira'),
			'parent_item_colon' => ''
	);

	$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position'=>9,
			'supports'=>array('title', 'thumbnail')


	);

	register_post_type( 'parceiro' , $args );

}


