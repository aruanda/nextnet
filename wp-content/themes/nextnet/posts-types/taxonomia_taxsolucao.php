<?php
add_action('init', 'taxsolucao_register');

function taxsolucao_register(){

	$argsCategoria = array(

			'labels'=>array(
				'name' => __('Grupo de Solução'),
				'singular_name' => __('taxsolucao'),
				'add_new' => __('Novo Grupo de Solução'),
				'add_new_item' => __('Adicionar novo Grupo de Solução'),
				'edit_item' => __('Editar Grupo de Solução'),
				'new_item' => __('Novo Grupo de Solução'),
				'view_item' => __('Ver Grupo de Solução'),
				'search_items' => __('Buscar Grupo de Solução'),
			),
			'hierarchical'=>true,


	);

	register_taxonomy('taxsolucao', array('servico' , 'solucao'), $argsCategoria);
}