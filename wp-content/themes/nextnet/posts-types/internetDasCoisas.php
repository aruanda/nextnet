<?php

add_action('init', 'internetDasCoisas_register');

function internetDasCoisas_register() {

	$labels = array(
			'name' => __('Internet das Coisas'),
			'singular_name' => __('internetDasCoisas'),
			'add_new' => __('Nova Publicação'),
			'add_new_item' => __('Adicionar nova Publicação'),
			'edit_item' => __('Editar Publicação'),
			'new_item' => __('Novo Publicação'),
			'view_item' => __('Ver Publicação'),
			'search_items' => __('Buscar Publicação'),
			'not_found' =>  __('Nenhuma Publicação encontrado'),
			'not_found_in_trash' => __('Nada encontrado na Lixeira'),
			'parent_item_colon' => ''
	);

	$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position'=>5,
			// 'taxonomies'=>array(),
			'supports'=>array('title', 'editor', 'thumbnail', 'excerpt')


	);

	register_post_type( 'internetDasCoisas' , $args );

}

