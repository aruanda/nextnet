<?php

add_action('init', 'blog_register');

function blog_register() {

	$labels = array(
			'name' => __('Blog Nextnet'),
			'singular_name' => __('blog'),
			'add_new' => __('Nova Publicação'),
			'add_new_item' => __('Adicionar nova Publicação'),
			'edit_item' => __('Editar Publicação'),
			'new_item' => __('Nova Publicação'),
			'view_item' => __('Ver Publicação'),
			'search_items' => __('Buscar Publicação'),
			'not_found' =>  __('Nenhuma Publicação encontrado'),
			'not_found_in_trash' => __('Nada encontrado na Lixeira'),
			'parent_item_colon' => ''
	);

	$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position'=>6,
			'taxonomies'=>array('categoria'),
			'supports'=>array('title', 'editor', 'thumbnail', 'excerpt')
	);

	register_post_type( 'blog' , $args );

}

