<?php


add_action('admin_menu', 'opcoes_do_tema');

function opcoes_do_tema() {
	add_menu_page('Dados da NextNet', 'Dados da NextNet', 'add_users', 'menu_dados_nextnet', 'menu_dados_nextnet',  get_bloginfo('url'). 6);
}

function menu_dados_nextnet() {
	
	if (isset($_POST['opt']) && !empty($_POST['opt'])) {
		foreach ($_POST['opt'] as $key => $value) {
			update_option($key, $value);
		}
	}
	
	echo '<style type="text/css">.mais-admin{margin:0 10px 0 0;width:32%;float:left;display:inline-block;}h2{padding: 10px;background: #84D5CE;color: white;}.option_field{width: 90%;padding: 10px;margin: 0 10px 10px 0;display: block;border: 1px solid #DDD;border-radius: 5px;background: #FCFCFC;} .option_field input, .option_field textarea, .option_field select { width:100%; } .option_field textarea { height: 130px; }</style>';
	echo '<form method="post">';

	echo '<div class="mais-admin">';
	
	echo '<h2>Dados da NextNet</h2>';
	
	echo '<h3>Dados da Empresa</h3>';
	field('text', 'endereco1', 'Endereço São Paulo');
	field('text', 'cep1', 'Cep São Paulo');
	field('text', 'telefone1', 'Telefone São Paulo');
	field('text', 'endereco2', 'Endereço Campo Grande');
	field('text', 'cep2', 'Cep Campo Grande');
	field('text', 'telefone2', 'Telefone Campo Grande');
	// field('text', 'email', 'E-mail');
	field('text', 'facebook', 'Facebook');
	field('text', 'twitter', 'Twitter');
	field('text', 'google', 'Google Plus');
	field('text', 'instagram', 'Instagram');
	field('text', 'linkedin', 'Linkedin');
	
	
	echo '<div class="clear"></div>';
	echo '<input type="submit" class="button-primary" value="Salvar" />';
	echo '</form>';
}

function field($type, $name, $title = '', $description = '', $options = array()) {
	$field = '<div class="option_field">' . $title . '<br />';
	switch ($type) :
		case ('text') : $field .= '<input type="text" name="opt[' . $name . ']" value="' . get_option($name) . '" />'; break;
		case ('hidden') : $field .= '<input type="hidden" name="opt[' . $name . ']" value="' . get_option($name) . '" />'; break;
		case ('select') :
			$field .= '<select name="opt[' . $name . ']">';
			foreach ($options as $key => $value) :
			if (get_option($name) == $key) : $sel = 'selected="selected"'; else : $sel = ''; endif;
			$field .= '<option value="' . $key . '" ' . $sel . '>' . $value . '</option>';
			endforeach;
			$field .= '</select>'; break;
		case ('textarea') : $field .= '<textarea name="opt[' . $name . ']">' . get_option($name) . '</textarea>'; break;
	endswitch;
	$field .= '<br /><small>' . $description . '</small>';
	$field .= '</div>';
	echo $field;
}