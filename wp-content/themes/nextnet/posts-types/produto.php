<?php

add_action('init', 'produto_register');

function produto_register() {

	$labels = array(
			'name' => __('Produtos'),
			'singular_name' => __('Produto'),
			'add_new' => __('Novo Produto'),
			'add_new_item' => __('Adicionar novo Produto'),
			'edit_item' => __('Editar Produto'),
			'new_item' => __('Novo Produto'),
			'view_item' => __('Ver Produto'),
			'search_items' => __('Buscar Produto'),
			'not_found' =>  __('Nenhum Produto encontrado'),
			'not_found_in_trash' => __('Nada encontrado na Lixeira'),
			'parent_item_colon' => ''
	);

	$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position'=>2,
			'taxonomies'=>array('tipo'),
			'supports'=>array('title', 'editor', 'thumbnail', 'excerpt')


	);

	register_post_type( 'produto' , $args );

}



