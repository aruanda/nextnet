<?php
add_action('init', 'seguimento_register');

function seguimento_register(){

	$argsCategoria = array(

			'labels'=>array(
				'name' => __('Seguimento'),
				'singular_name' => __('seguimento'),
				'add_new' => __('Novo Seguimento'),
				'add_new_item' => __('Adicionar novo Seguimento'),
				'edit_item' => __('Editar Seguimento'),
				'new_item' => __('Novo Seguimento'),
				'view_item' => __('Ver Seguimento'),
				'search_items' => __('Buscar Seguimento'),
			),
			'hierarchical'=>true,


	);

	register_taxonomy('tipo', array('solucao' , ''), $argsCategoria);
}