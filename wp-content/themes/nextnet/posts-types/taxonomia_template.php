<?php
add_action('init', 'template_register');

function template_register(){

	$argsCategoria = array(

			'labels'=>array(
				'name' => __('Template'),
				'singular_name' => __('template'),
				'add_new' => __('Novo Template'),
				'add_new_item' => __('Adicionar novo Template'),
				'edit_item' => __('Editar Tipo'),
				'new_item' => __('Novo Tipo'),
				'view_item' => __('Ver Tipo'),
				'search_items' => __('Buscar Tipo'),
			),
			'hierarchical'=>true,


	);

	register_taxonomy('template', array('portfolios' , ''), $argsCategoria);
}