<?php get_header();?>

<div class="row solucao-header-leitura servico-header-leitura">
 <?php
    $args = array(
        'post_type' => 'attachment',
        'numberposts' => -1,
        'post_status' => null,
        'post_parent' => $post->ID,
      'exclude'=>get_post_thumbnail_id(get_the_ID())

    );

    $attachments = get_posts( $args );
    if ( $attachments ) { foreach ($attachments as $attachment) { $link = wp_get_attachment_image_src($attachment->ID,array(308, 308)); ?>
    <div class="box-bg-solucao" style="background: url(<?php echo $link[0]; ?>);"></div>
  <?php } } ?>

  <div class="container-fluid">
    <div class="container">
      <h1><?php the_title(); ?></h1>
      <p><?php the_excerpt(); ?></p>
    </div>
  </div>
</div>

<div class="row solucao-cont-interna servico-cont-interna">
  <div class="container-fluid">
    <div class="container">
      <div class="cada-solucao-interna">
        <div class="cont-solucao-interna">
            <?php if ( have_posts() ) : the_post(); ?>

              <img src="<?php echo current(wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full'));?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" class="pull-right hidden-sm hidden-xs visible-md visible-lg">
            <?php the_content( ); ?>
        <?php endif; ?>
          <div class="box-servico-solucao">
            <h6>Soluções para o mercado</h6>
            <ul>
            <?php
                $args=array(
                    'taxonomy' => 'seguimento',
                    'order' => 'DESC'
                );
                $categories=get_categories($args);
                foreach($categories as $category) {
            ?>

             <?php query_posts(
                     array( 'post_type' => 'solucao',
                             'orderby' => 'title',
                             'order' => 'ASC',
                             'estado'=> $category->slug,
                             'showposts' => 3)
                     );
                 ?>

            <?php }  ?>

           <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
              <li>
                <img src="http://downloads.open4group.com/wallpapers/cata-ventos-na-industria-ea77f.jpg" alt="Server Cisco">
                <h3><?php the_title(); ?></h3>
                <p><?php the_excerpt(); ?></p>

                <a href="<?php the_permalink(); ?>" class="btn-transparent">veja mais</a>
              </li>
            <?php endwhile; ?>

            </ul>
          </div>
          <div class="box-contato-solucao">
              <div class="compartilhar-solucao">
                <i>Fale sobre esta solução: </i>
                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Facebook">
                  <span class="icon-facebook3"></span>
                </a>
                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Twitter">
                  <span class="icon-twitter3"></span>
                </a>
                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Google +">
                  <span class="icon-google-plus3"></span>
                </a>
                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Linkedin">
                  <span class="icon-linkedin-with-circle"></span>
                </a>
            </div>
            <div class="form-contato-solucao">
              <form action="">
                <textarea>Fale conosco...</textarea>
                <div class="campo-email">
                  <i class="glyphicon glyphicon-user"></i>
                  <input type="text" placeholder="seu email *">
                </div>
                <div class="campo-email">
                  <i class="glyphicon glyphicon-phone"></i>
                  <input type="text" placeholder="seu telefone">
                </div>
                <button type="button" class="btn-send"><span class="glyphicon glyphicon-send"></span></button>
              </form>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>


<?php get_footer(); ?>