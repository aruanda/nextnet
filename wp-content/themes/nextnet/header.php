<!doctype html>
<html ng-app="nurimbaSite">
  <head>
    <meta charset="utf-8">
    <title><?php wp_title('|', true, 'right'); ?></title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="robots" content="">
    <meta name="author" content="">
    <meta name="copyright" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta property="og:image" content="">
    <meta property="og:image:type" content="">
    <meta property="og:image:width" content="">
    <meta property="og:image:height" content="">

    <meta property="og:title" content="">
    <meta property="og:site_name" content="">
    <meta property="og:description" content="">

    <link rel="shortcut icon" href="img/favicon.ico" />

    <link rel="stylesheet" href="<?php bloginfo("template_url");?>/styles/vendor-ae914188a2.css">
   <link rel="stylesheet" href="<?php bloginfo("template_url");?>/styles/app-c9c90c26aa.css">
   <link rel="stylesheet" href="<?php bloginfo("template_url");?>/styles/app-6d12956f78.css">
   <link rel="stylesheet" href="<?php bloginfo("template_url");?>/styles/app-b6da440ef0.css">

    <?php wp_head(); ?>
  </head>
  <body>
    <header>
      <div class="row header">
        <div class="container-fluid">
          <div class="container">
            <div class="logo">
              <a href="<?php echo get_site_url(); ?>">
                <img src="<?php bloginfo("template_url");?>/img/logo.jpg" alt="<?php wp_title('|', true, 'right'); ?>">
              </a>
            </div>
            <nav class="menu-topo">
              <div class="menu-nav hidden-md hidden-lg">
                <span class="glyphicon glyphicon-menu-hamburger"></span>
              </div>
				<?php wp_nav_menu( array(
		             'menu' => 'Menu Topo',
					 'menu_class' => 'hidden-xs hidden-sm'
					  )
					);
				?>
            </nav>
          </div>
        </div>
      </div>

      <div class="row header hidden-md hidden-lg menu-link">
        <div class="container-fluid">
          <div class="container">
            <nav class="menu-topo">
				<?php wp_nav_menu( array(
		             'menu' => 'Menu Topo',
					  )
					);
				?>
            </nav>
          </div>
        </div>
      </div>

      <div id="header-fixed">
        <div class="row header">
          <div class="container-fluid">
            <div class="container">
              <div class="logo">
                <a href="index.php">
                <img src="<?php bloginfo("template_url");?>/img/logo.jpg" alt="<?php wp_title('|', true, 'right'); ?>">
                </a>
              </div>
              <nav class="menu-topo">
                <div class="menu-nav hidden-md hidden-lg">
                  <span class="glyphicon glyphicon-menu-hamburger"></span>
                </div>
				<?php wp_nav_menu( array(
		             'menu' => 'Menu Topo',
					 'menu_class' => 'hidden-xs hidden-sm'
					  )
					);
				?>
              </nav>
            </div>
          </div>
        </div>

        <div class="row header hidden-md hidden-lg menu-link">
          <div class="container-fluid">
            <div class="container">
              <nav class="menu-topo">
				<?php wp_nav_menu( array(
		             'menu' => 'Menu Topo',
					  )
					);
				?>
              </nav>
            </div>
          </div>
        </div>
      </div>

    </header>

