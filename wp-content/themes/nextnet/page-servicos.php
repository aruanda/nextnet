<?php
/*
 * Template Name: Serviços
 */
 ?>
<?php get_header();?>

<div class="row destaque-servico">
  <div class="container-fluid">
    <div class="container">
         <?php dynamic_sidebar ( 'sidebar-servicos' ) ; ?>
    </div>
  </div>
</div>

<div class="row servicos servicos-interna">
  <div class="container-fluid">
    <div class="container">
      <ul class="lista-servicos">
        <?php
                query_posts( array( 'post_type' => 'servico',  ));
                ?>
                <?php if ( have_posts() )

                global $wp_query;
                while ( have_posts() ) :
                    the_post();
                    $class = ( $count%2 == 0 ) ? 'servico-left' : 'servico-right';
                    $count++; ?>
            <li class="<?php post_class( $class ); ?>">
              <div class="img-servico">
                    <img src="<?php echo current(wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full'));?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" >
            </div>
              <div class="txt-servico">
                <h2><?php the_title(); ?></h2>
                <p><?php the_excerpt(); ?></p>
                <a href="<?php the_permalink(); ?>" class="btn-light">veja mais</a>
              </div>
            </li>
      <?php endwhile; ?>




      </ul>
    </div>
  </div>
</div>

<div class="row">
  <div class="container-fluid">
    <div class="container">

    </div>
  </div>
</div>


<?php get_footer(); ?>