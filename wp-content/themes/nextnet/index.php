<?php get_header();?>

    <div class="row destaque">
      <div class="container-fluid">
        <div class="container">
         <?php dynamic_sidebar ( 'sidebar-topo' ) ; ?>
        </div>
      </div>
    </div>

    <div class="row servicos">
      <div class="container-fluid">
        <div class="container">
          <ul class="lista-servicos">
            <?php  $args =  array('post_type' => 'servico', 'tax_query'=>array(array('taxonomy'=>'destaque', 'field'=>'slug', 'terms'=> array('capa'), 'operator'=>'IN', 'include_children '=>true)), 'posts_per_page'=>3, 'order'=>'DESC');
             query_posts( $args );
              ?>
           <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

            <li class="servico-left">
              <div class="img-servico">
                <img src="<?php echo current(wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full'));?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" >
              </div>
              <div class="txt-servico">
                <h2><?php the_title(); ?></h2>
                <p><?php the_excerpt(); ?></p>
                <a href="<?php the_permalink(); ?>" class="btn-light">veja mais</a>
              </div>
            </li>
          <?php endwhile; ?>

          </ul>
        </div>
      </div>
    </div>

<div class="row clientes">
  <div class="container-fluid">
    <div class="container">
      <div class="box-header-cliente">
           <?php dynamic_sidebar ( 'sidebar-clientes' ) ; ?>
      </div>
      <ul class="lista-clientes">
          <?php  $args =  array('post_type' => 'cliente', 'tax_query'=>array(array('taxonomy'=>'destaque', 'field'=>'slug', 'terms'=> array('capa'), 'operator'=>'IN', 'include_children '=>true)), 'posts_per_page'=>3, 'order'=>'DESC');
           query_posts( $args );
            ?>
         <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
        <li>
          <div class="box-img-cliente">
            <a href="<?php the_permalink(); ?>">
                <img src="<?php echo current(wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full'));?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" >
            </a>
          </div>
          <div class="box-cont-cliente">
            <h2><?php the_title(); ?></h2>
            <p><?php the_excerpt(); ?></p>
          </div>
        </li>
          <?php endwhile; ?>
      </ul>
      <a href="<?php echo get_site_url(); ?>/clientes" class="btn-transparent">veja mais</a>
    </div>
  </div>
</div>

    <div class="row solucoes">
      <div class="container-fluid">
        <div class="container">
           <?php dynamic_sidebar ( 'sidebar-solucoes' ) ; ?>
        </div>
      </div>
   </div>

   <div class="row cont-solucoes">
     <div class="container-fluid">
       <div class="container">
         <ul class="lista-solucoes">
      <?php wp_reset_query(); query_posts('post_type=solucao') ?>
      <?php
              global $wp_query;
              while ( have_posts() ) :  the_post();
         ?>

           <li class="solucao-gray<?php echo contador(); ?>">
             <a href="<?php the_permalink();  ?>">
                <h2><?php the_title(); ?></h2>
                <p><?php the_excerpt(); ?></p>
             </a>
           </li>

       <?php endwhile; ?>

         </ul>
         <a href="#" class="prev_btn"><span class="glyphicon glyphicon-menu-left"></span></a>
         <a href="#" class="next_btn"><span class="glyphicon glyphicon-menu-right"></span></a>
       </div>
     </div>
  </div>

  <div class="row internet-capa">
  <div class="layer-video"></div>
  <video autoplay="" loop="" poster="<?php bloginfo("template_url");?>img/video-capa.jpg" id="bgvid" muted>
    <source src="<?php bloginfo("template_url");?>/img/internetofthings.mp4" type="video/mp4">
  </video>
    <div class="container-fluid">
      <div class="container">
         <?php dynamic_sidebar ( 'sidebar-internet' ) ; ?>
        <a href="<?php echo get_site_url(); ?>/internet-das-coisas" class="btn-light">veja mais</a>
      </div>
    </div>
  </div>

  <div class="row posts-recentes">
    <div class="container-fluid">
      <div class="container">
        <ul>
            <?php  $args =  array('post_type' => 'blog', 'tax_query'=>array(array('taxonomy'=>'destaque', 'field'=>'slug', 'terms'=> array('capa'), 'operator'=>'IN', 'include_children '=>true)), 'posts_per_page'=>3, 'order'=>'DESC');
             query_posts( $args );
              ?>
           <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
            <li>
            <div class="box-cont-img">
            <?php
              $terms = get_the_terms($post->ID, 'categoria' );
              if ($terms && ! is_wp_error($terms)) :
                  $term_names_arr = array();
                  foreach ($terms as $term) {
                      $term_names_arr[] = $term->name;
                  }
                  $nomeTipo = join( " ", $term_names_arr);
              endif;
             ?>
              <a href="<?php the_permalink(); ?>">
                <span class="bullet"><?php echo $nomeTipo; ?></span>
                <img src="<?php echo current(wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full'));?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
              </a>
            </div>
            <div class="box-cont-post">
                <h2><?php the_title(); ?></h2>
                <p><?php the_excerpt(); ?></p>
              <a href="<?php the_permalink(); ?>" class="btn-transparent">veja mais</a>
            </div>
          </li>
          <?php endwhile; ?>
        </ul>
      </div>
    </div>
  </div>

  <div class="row parceiros">
    <div class="container-fluid">
      <div class="container">
        <div class="box-txt-parceiros">
         <?php dynamic_sidebar ( 'sidebar-parceiros' ) ; ?>
          <a href="<?php echo get_site_url(); ?>/parceiros" class="btn-transparent">veja mais</a>
        </div>
        <ul class="lista-parceiros">
            <?php  $args =  array('post_type' => 'parceiro', 'tax_query'=>array(array('taxonomy'=>'destaque', 'field'=>'slug', 'terms'=> array('capa'), 'operator'=>'IN', 'include_children '=>true)), 'posts_per_page'=>3, 'order'=>'DESC');
             query_posts( $args );
              ?>
           <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
          <li>
            <a href="<?php the_permalink(); ?>">
                <img src="<?php echo current(wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full'));?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" >
            </a>
          </li>
          <?php endwhile; ?>
        </ul>
      </div>
    </div>
  </div>



<?php get_footer(); ?>